<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>DWG Company</title>

	<link rel="shortcut icon" href="assets/ico/favicon.ico">

	<!-- Bootstrap core CSS and Custom styles-->
	<link href="assets/css/bootstrap.css" rel="stylesheet">
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/main.css" rel="stylesheet">
	<link href="assets/css/font-awesome.min.css" rel="stylesheet">

	<!-- Modernizr -->
	<script src="assets/js/modernizr.js"></script>

</head>

<body>

	<!-- Fixed navbar -->
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.php">DWG COMPANY</a>
			</div>
			<div class="navbar-collapse collapse navbar-right">
				<ul class="nav navbar-nav">
					<li><a href="index.php">Home</a></li>
					<li><a href="noticias.php">Notícias</a></li>
					<li><a href="parceiros.php">Parceiros</a></li>
					<li><a href="contato.php">Contato</a></li>
					<li class="dropdown hidden">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Seu nome <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="#">Meus cadastro</a></li>
							<li><a href="#.html">Alterar senha</a></li>
							<li><a href="#.html">Sair</a></li>
						</ul>
					</li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div>