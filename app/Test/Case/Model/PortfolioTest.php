<?php
App::uses('Portfolio', 'Model');

/**
 * Portfolio Test Case
 *
 */
class PortfolioTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.portfolio',
		'app.status',
		'app.bio',
		'app.imagem',
		'app.usuario',
		'app.tipo',
		'app.log'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Portfolio = ClassRegistry::init('Portfolio');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Portfolio);

		parent::tearDown();
	}

}
