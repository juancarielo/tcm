<?php
App::uses('Parceiro', 'Model');

/**
 * Parceiro Test Case
 *
 */
class ParceiroTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.parceiro',
		'app.status',
		'app.bio',
		'app.cliente',
		'app.portfolio',
		'app.imagem',
		'app.contato',
		'app.usuario',
		'app.tipo',
		'app.log'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Parceiro = ClassRegistry::init('Parceiro');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Parceiro);

		parent::tearDown();
	}

}
