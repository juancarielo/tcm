<?php
App::uses('Bio', 'Model');

/**
 * Bio Test Case
 *
 */
class BioTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.bio',
		'app.status'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Bio = ClassRegistry::init('Bio');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Bio);

		parent::tearDown();
	}

}
