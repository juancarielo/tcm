<?php
/**
 * ImagemFixture
 *
 */
class ImagemFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'status_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'portfolio_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'img' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_imagens_portfolios1_idx' => array('column' => 'portfolio_id', 'unique' => 0),
			'fk_imagens_status1_idx' => array('column' => 'status_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'status_id' => 1,
			'portfolio_id' => 1,
			'img' => 'Lorem ipsum dolor sit amet',
			'created' => '2014-05-21 23:14:57',
			'modified' => '2014-05-21 23:14:57'
		),
	);

}
