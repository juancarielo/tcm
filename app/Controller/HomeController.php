<?php

class HomeController extends AppController {

	public $uses = array('Noticia', 'Portfolio');

	public function index() {

		$bios = $this->Noticia->find('all', array('conditions' => array('Noticia.status_id <> ' => 2), 'limit' => '1'));
		#$portfolios = $this->Portfolio->find('all', array('conditions' => array('Portfolio.status_id <> ' => 2)));

		#$this->set(compact('bios', 'portfolios'));
		$this->set(compact('noticias'));

	}// Fim index.

}