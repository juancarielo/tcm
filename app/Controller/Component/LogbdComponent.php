<?php 
/**
 * Logs
 * @property Log $LogModel
 */
class LogbdComponent extends Component
{

	private $Controller;
	private $LogModel;

	public function initialize(Controller $Controller) {	
		parent::initialize($Controller);
		$this->Controller = $Controller;
	}

	/**
     * Registra os logs no banco de dados
     * 
     * @param int $usuario_id
     * @param string $tabela
     * @param string $registro_id
     * @param string $operacao
     * @param string $observacoes
     */
	public function registrar($usuario_id = null, $tabela = null, $registro_id = null, $operacao = null, $observacoes = null)
	{


		$dados_usuario = array(
			'clientIp' => $this->Controller->request->clientIp(),
			'referer' => $this->Controller->request->referer(),
			'method' => $this->Controller->request->method(),
			'userAgent' => $this->Controller->request->header('User-Agent'),
		);
		
		$this->LogModel = ClassRegistry::init('Log');
		$this->LogModel->create();
		$this->LogModel->save(array(
			'usuario_id' => $usuario_id, 
			'tabela' => $tabela, 
			'registro_id' => $registro_id, 
			'operacao' => $operacao, 
			'dados_usuario' => json_encode($dados_usuario), 
			'observacoes' => $observacoes
		));
	}
}