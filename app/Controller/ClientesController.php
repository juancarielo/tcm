<?php
App::uses('AppController', 'Controller');
/**
 * Clientes Controller
 *
 * @property Cliente $Cliente
 * @property PaginatorComponent $Paginator
 */
class ClientesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Cliente->recursive = 0;
		$this->Paginator->settings = array(
	        'conditions' => array('Cliente.status_id <> ' => 3),
	    );
		$this->set('clientes', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Cliente->exists($id)) {
			throw new NotFoundException(__('Invalid cliente'));
		}
		$options = array('conditions' => array('Cliente.' . $this->Cliente->primaryKey => $id));
		$this->set('cliente', $this->Cliente->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Cliente->create();
			$this->request->data['Cliente']['status_id'] = 1;
			if ($this->Cliente->save($this->request->data)) {
				$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'clientes', $this->Cliente->id, 'insert');
				$this->Session->setFlash(__('The cliente has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cliente could not be saved. Please, try again.'), 'warning');
			}
		}
		$status = $this->Cliente->Status->find('list');
		$this->set(compact('status'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Cliente->exists($id)) {
			throw new NotFoundException(__('Invalid cliente'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Cliente->save($this->request->data)) {
				$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'clientes', $this->Cliente->id, 'update');
				$this->Session->setFlash(__('The cliente has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cliente could not be saved. Please, try again.'), 'warning');
			}
		} else {
			$options = array('conditions' => array('Cliente.' . $this->Cliente->primaryKey => $id));
			$this->request->data = $this->Cliente->find('first', $options);
		}
		$status = $this->Cliente->Status->find('list');
		$this->set(compact('status'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Cliente->id = $id;
		if (!$this->Cliente->exists()) {
			throw new NotFoundException(__('Invalid cliente'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Cliente->saveField('status_id', 3)) {
			$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'clientes', $this->Cliente->id, 'delete');
			$this->Session->setFlash(__('The cliente has been deleted.'), 'info');
		} else {
			$this->Session->setFlash(__('The cliente could not be deleted. Please, try again.'), 'warning');
		}
		return $this->redirect(array('action' => 'index'));
	}}
