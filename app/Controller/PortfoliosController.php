<?php
App::uses('AppController', 'Controller');
/**
 * Portfolios Controller
 *
 * @property Portfolio $Portfolio
 * @property PaginatorComponent $Paginator
 */
class PortfoliosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Portfolio->recursive = 0;
		$this->Paginator->settings = array(
	        'conditions' => array('Portfolio.status_id <> ' => 3),
	    );
		$this->set('portfolios', $this->Paginator->paginate());
		$this->view = '/Elements/portfolios/index';
	} // Fim index.

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view() {
		if (!$this->Portfolio->exists($this->params['slug'])) {
			#throw new NotFoundException(__('Invalid portfolio'));
		}
		$options = array('conditions' => array('Portfolio.slug' => $this->params['slug']));
		$this->set('portfolio', $this->Portfolio->find('first', $options));
	} // Fim view.

/**
 * [admin_view_ajax description]
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
	public function admin_view_ajax($id){

		// $this->autoRender = false;
		$this->layout ='ajax';
		$options = array('conditions' => array('Portfolio.' . $this->Portfolio->primaryKey => $id));
		$portfolio = $this->Portfolio->find('first', $options);

		$this->set('portfolio',$portfolio);
	} // Fim admin_view_ajax.


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Portfolio->recursive = 0;
		$this->Paginator->settings = array(
	        'conditions' => array('Portfolio.status_id <> ' => 3),
	    );
		$this->set('portfolios', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Portfolio->exists($id)) {
			throw new NotFoundException(__('Invalid portfolio'));
		}
		$options = array('conditions' => array('Portfolio.' . $this->Portfolio->primaryKey => $id));
		$this->set('portfolio', $this->Portfolio->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Portfolio->create();
			$this->request->data['Portfolio']['status_id'] = 1;
			if ($this->Portfolio->save($this->request->data)) {
				$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'portfolios', $this->Portfolio->id, 'insert');
				$this->Session->setFlash(__('The portfolio has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The portfolio could not be saved. Please, try again.'), 'warning');
			}
		}
		$status = $this->Portfolio->Status->find('list');
		$clientes = $this->Portfolio->Cliente->find('list');
		$this->set(compact('status', 'clientes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Portfolio->exists($id)) {
			throw new NotFoundException(__('Invalid portfolio'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Portfolio->save($this->request->data)) {
				$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'portfolios', $this->Portfolio->id, 'update');
				$this->Session->setFlash(__('The portfolio has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The portfolio could not be saved. Please, try again.'), 'warning');
			}
		} else {
			$options = array('conditions' => array('Portfolio.' . $this->Portfolio->primaryKey => $id));
			$this->request->data = $this->Portfolio->find('first', $options);
		}
		$status = $this->Portfolio->Status->find('list');
		$clientes = $this->Portfolio->Cliente->find('list');
		$this->set(compact('status', 'clientes'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Portfolio->id = $id;
		if (!$this->Portfolio->exists()) {
			throw new NotFoundException(__('Invalid portfolio'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Portfolio->saveField('status_id', 3)) {
			$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'portfolios', $this->Portfolio->id, 'delete');
			$this->Session->setFlash(__('The portfolio has been deleted.'), 'info');
		} else {
			$this->Session->setFlash(__('The portfolio could not be deleted. Please, try again.'), 'warning');
		}
		return $this->redirect(array('action' => 'index'));
	}}
