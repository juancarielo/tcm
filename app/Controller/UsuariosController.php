<?php
App::uses('AppController', 'Controller');
/**
 * Usuarios Controller
 *
 * @property Usuario $Usuario
 * @property PaginatorComponent $Paginator
 */
class UsuariosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Usuario->recursive = 0;
		$this->Paginator->settings = array(
	        'conditions' => array('Usuario.status_id <> ' => 3),
	    );
		$this->set('usuarios', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Usuario->exists($id)) {
			throw new NotFoundException(__('Invalid usuario'));
		}
		$options = array('conditions' => array('Usuario.' . $this->Usuario->primaryKey => $id));
		$this->set('usuario', $this->Usuario->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Usuario->create();
			$this->request->data['Usuario']['status_id'] = 1;
			if ($this->Usuario->save($this->request->data)) {
				$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'usuarios', $this->Usuario->id, 'insert');
				$this->Session->setFlash(__('The usuario has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The usuario could not be saved. Please, try again.'), 'warning');
			}
		}
		$status = $this->Usuario->Status->find('list');
		$tipos = $this->Usuario->Tipo->find('list');
		$this->set(compact('status', 'tipos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Usuario->exists($id)) {
			throw new NotFoundException(__('Invalid usuario'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Usuario->save($this->request->data)) {
				$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'usuarios', $this->Usuario->id, 'update');
				$this->Session->setFlash(__('The usuario has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The usuario could not be saved. Please, try again.'), 'warning');
			}
		} else {
			$options = array('conditions' => array('Usuario.' . $this->Usuario->primaryKey => $id));
			$this->request->data = $this->Usuario->find('first', $options);
		}
		$status = $this->Usuario->Status->find('list');
		$tipos = $this->Usuario->Tipo->find('list');
		$this->set(compact('status', 'tipos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Usuario->id = $id;
		if (!$this->Usuario->exists()) {
			throw new NotFoundException(__('Invalid usuario'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Usuario->saveField('status_id', 3)) {
			$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'usuarios', $this->Usuario->id, 'delete');
			$this->Session->setFlash(__('The usuario has been deleted.'), 'info');
		} else {
			$this->Session->setFlash(__('The usuario could not be deleted. Please, try again.'), 'warning');
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Usuario->recursive = 0;
		$this->Paginator->settings = array(
	        'conditions' => array('Usuario.status_id <> ' => 3),
	    );
		$this->set('usuarios', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Usuario->exists($id)) {
			throw new NotFoundException(__('Invalid usuario'));
		}
		$options = array('conditions' => array('Usuario.' . $this->Usuario->primaryKey => $id));
		$this->set('usuario', $this->Usuario->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Usuario->create();
			$this->request->data['Usuario']['status_id'] = 1;
			if ($this->Usuario->save($this->request->data)) {
				$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'usuarios', $this->Usuario->id, 'insert');
				$this->Session->setFlash(__('The usuario has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The usuario could not be saved. Please, try again.'), 'warning');
			}
		}
		$status = $this->Usuario->Status->find('list');
		$tipos = $this->Usuario->Tipo->find('list');
		$this->set(compact('status', 'tipos'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Usuario->exists($id)) {
			throw new NotFoundException(__('Invalid usuario'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Usuario->save($this->request->data)) {
				$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'usuarios', $this->Usuario->id, 'update');
				$this->Session->setFlash(__('The usuario has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The usuario could not be saved. Please, try again.'), 'warning');
			}
		} else {
			$options = array('conditions' => array('Usuario.' . $this->Usuario->primaryKey => $id));
			$this->request->data = $this->Usuario->find('first', $options);
		}
		$status = $this->Usuario->Status->find('list');
		$tipos = $this->Usuario->Tipo->find('list');
		$this->set(compact('status', 'tipos'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Usuario->id = $id;
		if (!$this->Usuario->exists()) {
			throw new NotFoundException(__('Invalid usuario'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Usuario->saveField('status_id', 3)) {
			$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'usuarios', $this->Usuario->id, 'delete');
			$this->Session->setFlash(__('The usuario has been deleted.'), 'info');
		} else {
			$this->Session->setFlash(__('The usuario could not be deleted. Please, try again.'), 'warning');
		}
		return $this->redirect(array('action' => 'index'));
	}}
