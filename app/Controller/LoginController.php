<?php

class LoginController extends AppController {

	public $uses = array('Usuario');

	public function index() {

		$this->layout = 'admin';

		if($this->Session->read('UsuarioLogado') || $this->Cookie->read('Cookie')) {
			$this->redirect(array('controller' => 'noticias', 'action' => 'index', 'admin' => true));
		}

		// Verifica e valida o usuário.
		if ($this->request->is('post')) {

			#debug($this->request->data);
			#exit();

			$usuario = $this->Usuario->find('first', array(
				'fields' => array('id', 'nome', 'status_id', 'tipo_id'),
				'conditions' => array(
					'Usuario.email' => $this->request->data['Usuario']['login'],
					'Usuario.senha' => md5($this->request->data['Usuario']['senha']),
					'Usuario.status_id' => 1,
					)
				)
			);

			// Se o usuário existir.
			if(count($usuario) > 0) {

				if ($this->request->data['Usuario']['manter'] == 1)
					$this->Cookie->write('Cookie', $usuario['Usuario']);

				$this->Session->write('UsuarioLogado', $usuario['Usuario']);
				$this->redirect(array('controller' => 'noticias', 'action' => 'index', 'admin' => true));

			} else {

				$this->Session->setFlash(('Login ou senha incorretos!'), 'default', array('class' => ''));

			}

		}

	}

	public function sair() {

		$this->Session->destroy();
		$this->Session->setFlash(('Sessão finalizada com sucesso!'), 'default', array('class' => ''));
		$this->redirect('/admin');

	}

} // Fim class.