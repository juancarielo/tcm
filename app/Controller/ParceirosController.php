<?php
App::uses('AppController', 'Controller');
/**
 * Parceiros Controller
 *
 * @property Parceiros $Parceiros
 * @property PaginatorComponent $Paginator
 */
class ParceirosController extends AppController {

	public $uses = array('Parceiro', 'Status');

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function index() {
		$this->Parceiro->recursive = 0;
		$this->Paginator->settings = array(
	        'conditions' => array('Parceiro.status_id <> ' => 3),
	    );
	    $this->set('parceiros', $this->Paginator->paginate());
	    $this->view = '/Elements/parceiros/index';
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Parceiro->recursive = 0;
		$this->Paginator->settings = array(
	        'conditions' => array('Parceiro.status_id <> ' => 3),
	    );
	    $this->set('parceiros', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Parceiro->exists($id)) {
			throw new NotFoundException(__('Invalid bio'));
		}
		$options = array('conditions' => array('Bio.' . $this->Parceiro->primaryKey => $id));
		$this->set('bio', $this->Parceiro->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Parceiro->create();
			$this->request->data['Parceiro']['status_id'] = 1;
			if ($this->Parceiro->save($this->request->data)) {
				$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'bios', $this->Parceiro->id, 'insert');
				$this->Session->setFlash(__('O parceiro foi salvo com sucesso!'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bio could not be saved. Please, try again.'), 'warning');
			}
		}
		$status = $this->Status->find('list');
		$this->set(compact('status'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Parceiro->exists($id)) {
			throw new NotFoundException(__('Invalid bio'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Parceiro->save($this->request->data)) {
				$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'bios', $this->Parceiro->id, 'update');
				$this->Session->setFlash(__('O parceiro foi salvo com sucesso!'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bio could not be saved. Please, try again.'), 'warning');
			}
		} else {
			$options = array('conditions' => array('Parceiro.' . $this->Parceiro->primaryKey => $id));
			$this->request->data = $this->Parceiro->find('first', $options);
		}
		$status = $this->Parceiro->Status->find('list');
		$this->set(compact('status'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Parceiro->id = $id;
		if (!$this->Parceiro->exists()) {
			throw new NotFoundException(__('Invalid bio'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Parceiro->saveField('status_id', 3)) {
			$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'bios', $this->Parceiro->id, 'delete');
			$this->Session->setFlash(__('O parceiro foi excluído com sucesso!'), 'info');
		} else {
			$this->Session->setFlash(__('The bio could not be deleted. Please, try again.'), 'warning');
		}
		return $this->redirect(array('action' => 'index'));
	}}
