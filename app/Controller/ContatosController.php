<?php

App::uses('AppController', 'Controller');

/**
 * Contatos Controller
 *
 * @property Contato $Contato
 * @property PaginatorComponent $Paginator
 */
class ContatosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'RequestHandler');

	public function index() {

		$this->set('title_for_layout', 'Contato');

		// Dados para o disparo de e-mail.
		setlocale(LC_ALL, "pt_BR", "pt_BR.iso-8859-1", "pt_BR.utf-8", "portuguese");
		date_default_timezone_set('America/Sao_Paulo');
		$data = date('F j, Y');
		$data = strftime('%B %d, %Y', strtotime( $data ));

		if ($this->request->is('post') || $this->request->is('put')) {

			$nome = $this->request->data['Contato']['nome'];
			$email_c = $this->request->data['Contato']['email'];
			$assunto = $this->request->data['Contato']['assunto'];
			$mensagem = $this->request->data['Contato']['mensagem'];

			//Verifica se está vazio os campos obrigatórios
			if($nome == "" or $email_c == "" or $mensagem == "") {

				$this->Session->setFlash('Campos vazios, preencha-os.', 'danger');

			} else {

				// Insere no banco.
				$this->Contato->create();
				$this->Contato->save(array(
					'status_id' => 1,
					'nome' => $nome,
					'email' => $email_c,
					'assunto' => $assunto,
					'mensagem' => $mensagem,
					'ip'=> $_SERVER["REMOTE_ADDR"],
					'user_agent'=> $_SERVER['HTTP_USER_AGENT']
					));

				App::uses('CakeEmail', 'Network/Email');

				$email = new CakeEmail('newcode');

				$email->to($email_c);
				$email->replyTo(array($email_c => $nome));
				#$email->bcc('contato@newcod3.com.br');

				$email->emailFormat('html');
				$email->subject('Contato');
				$email->template('contato');
				$email->viewVars(array('titulo'=>'Contato', 'nome' => $nome, 'email' => $email_c, 'assunto' => $assunto, 'mensagem' => nl2br($mensagem), 'data' => $data));

				$email->send();

				$this->Session->setFlash('Sua mensagem foi enviada com sucesso, obrigado!', 'success');
				$this->redirect(array('controller' => 'home', 'action' => 'index'));

			}

		} // Fim post.
	} // Fim index.

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {

		$this->Contato->recursive = 0;
		$this->Paginator->settings = array(
	        'conditions' => array('Contato.status_id <> ' => 3),
	    );
		$this->set('contatos', $this->Paginator->paginate());
	}

/**
 * [admin_exportar_csv description]
 * @return [type] [description]
 */
    public function admin_exportar(){

		App::import("Vendor", "PHPExcel");




		exit();


    }

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {

		if (!$this->Contato->exists($id)) {
			throw new NotFoundException(__('Invalid contato'));
		}

		$options = array('conditions' => array('Contato.' . $this->Contato->primaryKey => $id));
		$this->set('contato', $this->Contato->find('first', $options));

		if ($id > 0) {
			$this->Contato->id = $id;
			$this->Contato->saveField('status_id', 4);
		}

	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Contato->create();
			$this->request->data['Contato']['status_id'] = 1;
			if ($this->Contato->save($this->request->data)) {
				$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'contatos', $this->Contato->id, 'insert');
				$this->Session->setFlash(__('The contato has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contato could not be saved. Please, try again.'), 'warning');
			}
		}
		$status = $this->Contato->Status->find('list');
		$this->set(compact('status'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Contato->exists($id)) {
			throw new NotFoundException(__('Invalid contato'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Contato->save($this->request->data)) {
				$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'contatos', $this->Contato->id, 'update');
				$this->Session->setFlash(__('The contato has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contato could not be saved. Please, try again.'), 'warning');
			}
		} else {
			$options = array('conditions' => array('Contato.' . $this->Contato->primaryKey => $id));
			$this->request->data = $this->Contato->find('first', $options);
		}
		$status = $this->Contato->Status->find('list');
		$this->set(compact('status'));
	}

/**
 * admin_lida method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_lida($id = null) {
		$this->Contato->id = $id;
		if (!$this->Contato->exists()) {
			throw new NotFoundException(__('Invalid contato'));
		}
		if ($this->Contato->saveField('status_id', 2)) {
			$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'contatos', $this->Contato->id, 'delete');
			$this->Session->setFlash(__('A conversa foi marcada como lida.'), 'info');
		} else {
			$this->Session->setFlash(__('A conversa foi marcada como não lida.'), 'warning');
		}
		return $this->redirect(array('action' => 'index'));
	} // Fim admin_lida.

/**
 * admin_nao_lida method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_nao_lida($id = null) {
		$this->Contato->id = $id;
		if (!$this->Contato->exists()) {
			throw new NotFoundException(__('Invalid contato'));
		}
		if ($this->Contato->saveField('status_id', 1)) {
			$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'contatos', $this->Contato->id, 'delete');
			$this->Session->setFlash(__('A conversa foi marcada como não lida.'), 'info');
		} else {
			$this->Session->setFlash(__('A conversa foi marcada como lida.'), 'warning');
		}
		return $this->redirect(array('action' => 'index'));
	} // Fim admin_nao_lida.

} // Fim class.