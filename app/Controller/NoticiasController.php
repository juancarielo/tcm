<?php
App::uses('AppController', 'Controller');
/**
 * Noticias Controller
 *
 * @property Noticias $Noticias
 * @property PaginatorComponent $Paginator
 */
class NoticiasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function index() {
		$this->Noticia->recursive = 0;
		$this->Paginator->settings = array(
	        'conditions' => array('Noticia.status_id <> ' => 3),
	    );
	    $this->set('noticias', $this->Paginator->paginate());
	    $this->view = '/Elements/noticias/index';
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ler($id = null) {
		if (!$this->Noticia->exists($id)) {
			throw new NotFoundException(__('Invalid noticia'));
		}
		$options = array('conditions' => array('Noticia.' . $this->Noticia->primaryKey => $id));
		$this->set('noticia', $this->Noticia->find('first', $options));
		$this->view = '/Elements/noticias/ler';
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Noticia->recursive = 0;
		$this->Paginator->settings = array(
	        'conditions' => array('Noticia.status_id <> ' => 3),
	    );
	    $this->set('noticias', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Noticia->exists($id)) {
			throw new NotFoundException(__('Invalid bio'));
		}
		$options = array('conditions' => array('Bio.' . $this->Noticia->primaryKey => $id));
		$this->set('bio', $this->Noticia->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Noticia->create();
			$this->request->data['Noticia']['status_id'] = 1;
			if ($this->Noticia->save($this->request->data)) {
				$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'bios', $this->Noticia->id, 'insert');
				$this->Session->setFlash(__('A notícia foi salva com sucesso!'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bio could not be saved. Please, try again.'), 'warning');
			}
		}
		$status = $this->Noticia->Status->find('list');
		$this->set(compact('status'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Noticia->exists($id)) {
			throw new NotFoundException(__('Invalid bio'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Noticia->save($this->request->data)) {
				$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'bios', $this->Noticia->id, 'update');
				$this->Session->setFlash(__('A notícia foi salva com sucesso!'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bio could not be saved. Please, try again.'), 'warning');
			}
		} else {
			$options = array('conditions' => array('Noticia.' . $this->Noticia->primaryKey => $id));
			$this->request->data = $this->Noticia->find('first', $options);
		}
		$status = $this->Noticia->Status->find('list');
		$this->set(compact('status'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Noticia->id = $id;
		if (!$this->Noticia->exists()) {
			throw new NotFoundException(__('Invalid bio'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Noticia->saveField('status_id', 3)) {
			$this->Logbd->registrar($this->Session->read("UsuarioLogado.id"), 'bios', $this->Noticia->id, 'delete');
			$this->Session->setFlash(__('A notícia foi excluída com sucesso!'), 'info');
		} else {
			$this->Session->setFlash(__('The bio could not be deleted. Please, try again.'), 'warning');
		}
		return $this->redirect(array('action' => 'index'));
	}}
