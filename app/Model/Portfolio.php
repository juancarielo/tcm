<?php
App::uses('AppModel', 'Model');
/**
 * Portfolio Model
 *
 * @property Status $Status
 * @property Cliente $Cliente
 * @property Imagem $Imagem
 */
class Portfolio extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'status_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		/*'cliente_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),*/
		'titulo' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'descricao' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'url' => array(
			'url' => array(
				'rule' => array('url'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'slug' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		/*'img' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Status' => array(
			'className' => 'Status',
			'foreignKey' => 'status_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cliente' => array(
			'className' => 'Cliente',
			'foreignKey' => 'cliente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Imagem' => array(
			'className' => 'Imagem',
			'foreignKey' => 'portfolio_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

/**
 * beforeSave
 *
 * @var array
 */
	public function beforeSave($options = array()) {


		/*if ($this->data['Cliente']['descricao']) {

			$this->Cliente->create();

			$this->data['Cliente']['status_id'] = 1;

			debug($this->data);
			exit();

		}*/

		// Transforma o título em slug.
		$this->data['Portfolio']['slug'] = Inflector::slug(strtolower($this->data['Portfolio']['titulo']), $replacement = '-');




		if(isset($this->data['Portfolio']['img']) && is_uploaded_file($this->data['Portfolio']['img']['tmp_name'])) {

			// Tratar imagem, job e cliente.
			$this->imgTmp = $this->data['Portfolio']['img'];
			$this->data['Portfolio']['img'] = $this->data['Portfolio']['img']['name'];

			// Pega dimensões da imgagem.
			$imgSize = getimagesize($this->data['Peca']['arquivo']['tmp_name']);

        }





        if(isset($this->data['Portfolio']['img_nova']) && is_uploaded_file($this->data['Portfolio']['img_nova']['tmp_name'])) {
        	$this->imgTmp = $this->data['Portfolio']['img'];
			$this->imgNovaTmp = $this->data['Portfolio']['img_nova'];
        }




        return true;

	} // Fim beforeSave.

/**
 * afterSave
 *
 * @var array
 */
	public function afterSave($created, $options = array()) {

		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');
		parent::afterSave($created, $options);

       	// Infos.
       	$webroot = WWW_ROOT.'files'.DS.'portfolios';

		//Cria pasta de acordo com o id do usuário.
		if(!is_dir($webroot.DS.$this->id)){
			new Folder($webroot.DS.$this->id, true, 0755);
		}

		// Enviar arquivos caso exista.
		if(isset($this->imgTmp) && is_uploaded_file($this->imgTmp['tmp_name'])) {

			// Monta o nome da imagem.
			$imgNome = md5(date("d/m/Y H:i:s"));
        	$imgExtension = pathinfo($this->imgTmp['name'], PATHINFO_EXTENSION);
        	$imgFull = $imgNome.'.'.$imgExtension;

        	// Define o caminho.
        	$pasta = $webroot.DS.$this->id.DS.$imgFull;
        	$img = 'files'.DS.'portfolios'.DS.$this->id.DS.$imgFull;

        	// Move o arquivo.
        	move_uploaded_file($this->imgTmp['tmp_name'], $pasta);

        	// Altera somente o campo arquivo.
        	$this->saveField('img', $img);

		}

		return true;

	} // Fim afterSave.

} // Fim class.