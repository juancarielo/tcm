<?php
App::uses('AppModel', 'Model');
/**
 * Tipo Model
 *
 * @property Usuario $Usuario
 */
class Tipo extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'descricao';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'tipo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
