<?php
App::uses('AppModel', 'Model');
/**
 * Noticia Model
 *
 * @property Status $Status
 */
class Noticia extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'status_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'titulo' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'descricao' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Status' => array(
			'className' => 'Status',
			'foreignKey' => 'status_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	/**
	 * [afterFind description]
	 * @param  [type]  $results [description]
	 * @param  boolean $primary [description]
	 * @return [type]           [description]
	 */
	/*function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
	        if (isset($val['Bio']['descricao'])) {
	            $results[$key]['Bio']['descricao'] = $this->substrStringAfterFind($val['Bio']['descricao']);
	        }
	    }
	    return $results;
	} // Fim beforeFilter
	*/

	/**
	 * [substrStringAfterFind description]
	 * @param  [type] $substrString [description]
	 * @return [type]               [description]
	 */
	/*
	function substrStringAfterFind($substrString){
		$substrString = substr($substrString, 0, 50);
		return $substrString.'...';
	} // Fim substrStringAfterFind.
	*/

} // Fim class.