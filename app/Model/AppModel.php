<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

	/**
	 * [afterFind description]
	 * @param  [type]  $results [description]
	 * @param  boolean $primary [description]
	 * @return [type]           [description]
	 */
	function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
			if (isset($val[$this->name]['created'])) {
				$results[$key][$this->name]['created'] = $this->datetimeFormatAfterFind($val[$this->name]['created']);
			} if (isset($val[$this->name]['modified'])) {
				$results[$key][$this->name]['modified'] = $this->datetimeFormatAfterFind($val[$this->name]['modified']);
			} /*if (isset($val[$this->name]['descricao'])) {
	            $results[$key][$this->name]['descricao'] = $this->substrStringAfterFind($val[$this->name]['descricao']);
	        }*/
		}
		return $results;
	} // Fim afterFind.

	/**
	 * [datetimeFormatAfterFind description]
	 * @param  [type] $datetimeString [description]
	 * @return [type]                 [description]
	 */
	function datetimeFormatAfterFind($datetimeString) {
		return date('d/m/Y', strtotime($datetimeString));
	} // Fim datetimeFormatAfterFind.

	/**
	 * [substrStringAfterFind description]
	 * @param  [type] $substrString [description]
	 * @return [type]               [description]
	 */
	function substrStringAfterFind($substrString){
		$substrString = substr($substrString, 0, 50);
		return $substrString.'...';
	} // Fim substrStringAfterFind.

} // Fim class.