<div id="blue">

	<div class="container">

		<div class="row">

			<h3>Contato</h3>

		</div>

	</div>

</div>

<div class="container mtb">

	<div class="row">

		<div class="col-md-12">

			<h4>Entre em contato conosco</h4>
			<div class="hline"></div>

			<p>A sua opinião é de grande interesse para nós, entre em contato conosco para sugestões, reclamações e afins.</p>

			<br>

			<?php echo $this->Form->create('Contato', array(
				'action' => 'index',
				'role' => 'form',
				'inputDefaults' => array(
					'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
					'div' => array('class' => 'form-group'),
					'class' => 'form-control',
					'label' => array('class' => 'control-label'),
					'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
					))); ?>

				<div class="form-group">
					<label for="InputName1">Seu nome</label>
					<input type="text" class="form-control" name="data[Contato][nome]" id="exampleInputEmail1">
				</div>

				<div class="form-group">
					<label for="InputEmail1">E-mail</label>
					<input type="email" class="form-control" name="data[Contato][email]" id="exampleInputEmail1">
				</div>

				<div class="form-group">
					<label for="InputSubject1">Assunto</label>
					<input type="text" class="form-control" name="data[Contato][assunto]" id="exampleInputEmail1">
				</div>

				<div class="form-group">
					<label for="message1">Mensagem</label>
					<textarea class="form-control" id="message1" name="data[Contato][mensagem]" rows="3"></textarea>
				</div>

				<button type="submit" class="btn btn-theme">Enviar</button>
				<?php echo $this->Form->end(); ?>

			</form>

		</div>

	</div>

</div>