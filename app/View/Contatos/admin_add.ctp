<div class="container">

	<div class="row">
		
		<div class="col-md-2 actions">

			<h3><?php echo __('Actions'); ?></h3>
			
			<ul class="nav nav-pills nav-stacked">

								<li><?php echo $this->Html->link(__('List Contatos'), array('action' => 'index')); ?></li>
						<li><?php echo $this->Html->link(__('List Status'), array('controller' => 'status', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Status'), array('controller' => 'status', 'action' => 'add')); ?> </li>
			</ul>
			<!-- /.nav nav-pills nav-stacked -->

		</div>
		<!-- /.col-md-2 -->

		<div class="col-md-10">

			<?php 
			$this->Html->addCrumb('Home', '/admin');
			$this->Html->addCrumb(__('Contatos'), '/admin/Contatos');
			$this->Html->addCrumb(__('Add'));
			echo $this->Html->getCrumbList(array('class' => 'breadcrumb', 'lastClass' => 'active'));
			?> 
			<!-- /.breadcrumb -->

			<div class="contatos form">

				<?php echo $this->Form->create('Contato', array(
					'role' => 'form',
					'inputDefaults' => array(
						'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
						'div' => array('class' => 'form-group'),
						'class' => 'form-control',
						'label' => array('class' => 'control-label'),
						'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
						))); ?>

						<fieldset>
							<legend><?php echo __('Admin Add Contato'); ?></legend>
								<?php
		echo $this->Form->input('status_id');
		echo $this->Form->input('nome');
		echo $this->Form->input('email');
		echo $this->Form->input('assunto');
		echo $this->Form->input('mensagem');
		echo $this->Form->input('ip');
		echo $this->Form->input('user_agent');
	?>
						</fieldset>

						<div class="space"><?php echo $this->Form->button (__('Submit'),array('type' => 'submit', 'class' => 'btn btn-primary', 'div' => false, 'style' => 'margin-right: 10px;')); ?><?php echo $this->Html->link(__('Cancel'), array('action' => 'index'), array('class' => 'btn btn-primary', 'div' => false)); ?></div> 
<!-- /.space --><?php echo $this->Form->end(); ?>

					</div>
					<!-- /.form -->

				</div>
				<!-- /.col-md-10 -->

			</div>
			<!-- /.row -->

		</div>
		<!-- /.container -->

