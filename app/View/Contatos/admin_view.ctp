<div class="container">

	<div class="row">

		<div class="col-md-12">

			<?php
			$this->Html->addCrumb('Home', '/admin');
			$this->Html->addCrumb(__('Contatos'), '/admin/Contatos');
			$this->Html->addCrumb(__('View') . " [{$this->request->params['pass'][0]}]" );
			echo $this->Html->getCrumbList(array('class' => 'breadcrumb', 'lastClass' => 'active'));
			?>
			<!-- /.breadcrumb -->

			<div class="contatos view">

				<h2 class="space"><?php echo __('Contato'); ?></h2>

				<div class="panel panel-default space">

					<ul class="list-group">
						<li class='list-group-item'><strong><?php echo __('Id'); ?></strong>: <?php echo h($contato['Contato']['id']); ?></li>
						<li class='list-group-item'><strong><?php echo __('Status'); ?></strong>: <?php echo h($contato['Status']['descricao']); ?></li>
						<li class='list-group-item'><strong><?php echo __('Nome'); ?></strong>: <?php echo h($contato['Contato']['nome']); ?></li>
						<li class='list-group-item'><strong><?php echo __('Email'); ?></strong>: <?php echo h($contato['Contato']['email']); ?></li>
						<li class='list-group-item'><strong><?php echo __('Assunto'); ?></strong>: <?php echo h($contato['Contato']['assunto']); ?></li>
						<li class='list-group-item'><strong><?php echo __('Mensagem'); ?></strong>: <?php echo h($contato['Contato']['mensagem']); ?></li>
						<li class='list-group-item'><strong><?php echo __('Ip'); ?></strong>: <?php echo h($contato['Contato']['ip']); ?></li>
						<li class='list-group-item'><strong><?php echo __('User Agent'); ?></strong>: <?php echo h($contato['Contato']['user_agent']); ?></li>
						<li class='list-group-item'><strong><?php echo __('Created'); ?></strong>: <?php echo h($contato['Contato']['created']); ?></li>
					</ul>
					<!-- /.list-group -->

				</div>
				<!-- /.panel panel-default space -->

			</div>
			<!-- /.view -->


		</div>
		<!-- /.col-md-10 -->

	</div>
	<!-- /.row -->

</div>
<!-- /.container -->
