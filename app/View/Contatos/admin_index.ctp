<div class="container">

	<div class="row">

		<div class="col-md-12">

			<?php
			$this->Html->addCrumb('Home', '/admin');
			$this->Html->addCrumb(__('Contatos'));
			echo $this->Html->getCrumbList(array('class' => 'breadcrumb', 'lastClass' => 'active'));
			?>
			<!-- /.breadcrumb -->

			<h2 class="space">
				<div class="clearfix">
					<div class="pull-left">
						<?php echo __('Contatos'); ?>
					</div>

					<div class="pull-right">
					<!-- <small><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-floppy-disk"></span> Exportar'), array('action' => 'exportar'), array('escape' => false, 'class' => 'btn btn-default')); ?></small> -->
					</div>
				</div>
			</h2>

			<?php if (!$contatos): ?>
				<p><?php echo __('No records found.'); ?></p>
			<?php else: ?>

				<div class="contatos index">

					<div class="table-responsive">

						<table class="table table-condensed table-bordered">

							<thead>
								<tr>
									<th><?php echo $this->Paginator->sort('id'); ?></th>
									<th><?php echo $this->Paginator->sort('status_id'); ?></th>
									<th><?php echo $this->Paginator->sort('nome'); ?></th>
									<th><?php echo $this->Paginator->sort('email'); ?></th>
									<th><?php echo $this->Paginator->sort('assunto'); ?></th>
									<th><?php echo $this->Paginator->sort('mensagem'); ?></th>
									<th><?php echo $this->Paginator->sort('ip'); ?></th>
									<th><?php echo $this->Paginator->sort('user_agent'); ?></th>
									<th><?php echo $this->Paginator->sort('created'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>

							<tbody>
								<?php foreach ($contatos as $contato): ?>

									<?php
									if($contato['Status']['id'] == 2){
										$link = 'nao_lida';
										$color = 'green';
										$msg = 'Marcar como não lida';
										$icon = '<span class="glyphicon glyphicon-remove-circle"></span>';
									} else if ($contato['Status']['id'] == 1){
										$color = 'red';
										$link = 'lida';
										$msg = 'Marcar como lida';
										$icon = '<span class="glyphicon glyphicon-ok-circle"></span>';
									}
									?>
									<tr style="color:<?php echo $color;?>">
										<td><?php echo h($contato['Contato']['id']); ?></td>
										<td><?php echo h($contato['Status']['descricao']); ?></td>
										<td><?php echo h($contato['Contato']['nome']); ?></td>
										<td><?php echo h($contato['Contato']['email']); ?></td>
										<td><?php echo h($contato['Contato']['assunto']); ?></td>
										<td><?php echo h($contato['Contato']['mensagem']); ?></td>
										<td><?php echo h($contato['Contato']['ip']); ?></td>
										<td><?php echo h($contato['Contato']['user_agent']); ?></td>
										<td><?php echo h($contato['Contato']['created']); ?></td>
										<td class="actions">
											<?php echo $this->Html->link('<span class="glyphicon glyphicon-eye-open"></span>', array('action' => 'view', $contato['Contato']['id']), array('escape' => false, 'class' => 'btn btn-default btn-xs', 'rel' => 'tooltip', 'data-original-title' => 'Visualizar')); ?>
											<?php echo $this->Form->postLink($icon, array('action' => $link, $contato['Contato']['id']), array('escape' => false, 'class' => 'btn btn-default btn-xs', 'rel' => 'tooltip', 'data-original-title' => $msg)); ?>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>

						</table>
						<!-- .table -->

					</div>
					<!-- /.table-responsive -->

					<p>
						<?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')	));	?>
					</p>
					<!-- /.paginator-counter -->

					<ul class="pagination">
						<?php
						echo $this->Paginator->prev('«', array('tag' => 'li'), null, array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'a'));
						echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
						echo $this->Paginator->next('»', array('tag' => 'li'), null, array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'a'));
						?>
					</ul>
					<!-- /.pagination -->

				</div>
				<!-- /.index -->

			<?php endif; ?>
		</div>
		<!-- /.col-md-10 -->

	</div>
	<!-- /.row -->

</div>
<!-- /.container -->
