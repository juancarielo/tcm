<div id="blue">
	<div class="container">
		<div class="row">
			<h3>Notícia <?php echo $noticia['Noticia']['id']; ?></h3>
		</div>
	</div>
</div>

<div class="container mtb">

	<div class="row">

		<div class="col-md-12">

			<!-- <p><img class="img-responsive" src="http://placehold.it/1140x250" alt=""></p> -->

			<h3 class="ctitle"><?php echo $noticia['Noticia']['titulo']; ?></h3>

			<br>

			<p><?php echo nl2br($noticia['Noticia']['descricao']); ?></p>

			<div class="spacing"></div>

		</div>

	</div>

</div>