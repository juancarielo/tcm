<div id="blue">

	<div class="container">

		<div class="row">

			<h3>Notícias</h3>

		</div>

	</div>

</div>

<div class="container mtb">

	<div class="row">

		<div class="col-md-12">

			<div class="spacing"></div>

			<?php foreach ($noticias as $noticia): ?>

				<h3 class="ctitle"><?php echo $noticia['Noticia']['titulo']; ?></h3>

				<p><csmall2>Postado em: <?php echo $noticia['Noticia']['created']; ?>.</csmall2></p>

				<p><?php echo $this->Text->truncate($noticia['Noticia']['descricao'], 650, array('ending' => '...', 'exact' => true, 'html' => true)); ?></p>

				<p><?php echo $this->Html->link('[Leia mais]', array('action' => 'ler', $noticia['Noticia']['id'])); ?></p>

				<div class="hline"></div>

				<div class="spacing"></div>

			<?php endforeach ?>

		</div>

	</div>

</div>