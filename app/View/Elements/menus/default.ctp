<div class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo $this->base; ?>">DWG COMPANY</a>
		</div>
		<div class="navbar-collapse collapse navbar-right">
			<ul class="nav navbar-nav">

				<li <?php if($this->request->params['controller'] == 'home' && $this->request->params['action'] == in_array($this->request->params['action'], array('index', 'add', 'view', 'edit'))) echo 'class="active"'; ?> ><?php echo $this->Html->link('Home', array('controller' => 'home', 'action' => 'index')); ?></li>

				<li <?php if($this->request->params['controller'] == 'noticias' && $this->request->params['action'] == in_array($this->request->params['action'], array('index', 'add', 'view', 'edit'))) echo 'class="active"'; ?> ><?php echo $this->Html->link('Notícias', array('controller' => 'noticias', 'action' => 'index')); ?></li>

				<li <?php if($this->request->params['controller'] == 'parceiros' && $this->request->params['action'] == in_array($this->request->params['action'], array('index', 'add', 'view', 'edit'))) echo 'class="active"'; ?> ><?php echo $this->Html->link('Parceiros', array('controller' => 'parceiros', 'action' => 'index')); ?></li>

				<li <?php if($this->request->params['controller'] == 'contatos' && $this->request->params['action'] == in_array($this->request->params['action'], array('index', 'add', 'view', 'edit'))) echo 'class="active"'; ?> ><?php echo $this->Html->link('Contato', array('controller' => 'contatos', 'action' => 'index')); ?></li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Seu nome <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#">Meus cadastro</a></li>
						<li><a href="#.html">Alterar senha</a></li>
						<li><a href="#.html">Sair</a></li>
					</ul>
				</li>

			</ul>
		</div><!--/.nav-collapse -->
	</div>
</div>

<div class="navbar navbar-inverse navbar-static-top hidden">

	<div class="container">

		<div class="navbar-header">

			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<a class="navbar-brand" href="<?php echo $this->base; ?>">
				<?php echo $this->Html->image('logo.png', array('class' => 'img-responsive', 'style' => 'width: 300px; position: absolute; margin-top: -18px;')); ?>
			</a>

		</div>
		<!-- /.navbar-header -->

		<div class="navbar-collapse collapse">

			<ul class="nav navbar-nav navbar-right">

				<li <?php if($this->request->params['controller'] == 'portfolios' && $this->request->params['action'] == in_array($this->request->params['action'], array('index', 'add', 'view', 'edit'))) echo 'class="active"'; ?> ><?php echo $this->Html->link('Portfólio', array('controller' => 'portfolios', 'action' => 'index')); ?></li>

				<li <?php if($this->request->params['controller'] == 'bios' && $this->request->params['action'] == in_array($this->request->params['action'], array('index', 'add', 'view', 'edit'))) echo 'class="active"'; ?> ><?php echo $this->Html->link('Sobre', array('controller' => 'bios', 'action' => 'index')); ?></li>

				<li <?php if($this->request->params['controller'] == 'contatos' && $this->request->params['action'] == in_array($this->request->params['action'], array('index', 'add', 'view', 'edit'))) echo 'class="active"'; ?> ><?php echo $this->Html->link('Contato', array('controller' => 'contatos', 'action' => 'index')); ?></li>

			</ul>
			<!-- /.navbar-collapse -->

		</div>
		<!--/.nav-collapse -->

	</div>
	<!-- /.container -->

</div>
<!-- /.navbar -->