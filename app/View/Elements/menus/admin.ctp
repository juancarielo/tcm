<nav class="navbar navbar-fixed-top navbar-default" role="navigation">

	<div class="container">

		<div class="navbar-header">

			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
				<span class="sr-only"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<a class="navbar-brand" href="#">DWG</a>

		</div>
		<!-- /.navbar-header -->

		<div class="collapse navbar-collapse" id="navbar-collapse">

			<ul class="nav navbar-nav">

				<li <?php if($this->request->params['controller'] == 'noticias' && $this->request->params['action'] == in_array($this->request->params['action'], array('admin_index', 'admin_add', 'admin_view', 'admin_edit'))) echo 'class="active"'; ?> ><?php echo $this->Html->link('Notícias', array('controller' => 'noticias', 'action' => 'admin_index')); ?></li>

				<li <?php if($this->request->params['controller'] == 'parceiros' && $this->request->params['action'] == in_array($this->request->params['action'], array('admin_index', 'admin_add', 'admin_view', 'admin_edit'))) echo 'class="active"'; ?> ><?php echo $this->Html->link('Parceiros', array('controller' => 'parceiros', 'action' => 'admin_index')); ?></li>

				<li <?php if($this->request->params['controller'] == 'contatos' && $this->request->params['action'] == in_array($this->request->params['action'], array('admin_index', 'admin_add', 'admin_view', 'admin_edit'))) echo 'class="active"'; ?> ><?php echo $this->Html->link('Contatos', array('controller' => 'contatos', 'action' => 'admin_index')); ?></li>

			</ul>
			<!-- /.nav navbar-nav -->

			<ul class="nav navbar-nav navbar-right">

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><strong><?php echo $this->Session->read('UsuarioLogado.nome'); ?></strong> <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><?php echo $this->Html->link('Acessar site', array('controller' => 'home', 'action' => 'index', 'admin' => false), array('target' => '_blank')); ?></li>
						<li class="divider"></li>
						<li><?php echo $this->Html->link('Sair', array('controller' => 'login', 'action' => 'sair', 'admin' => false)); ?></li>
					</ul>
				</li>
				<!-- /.dropdown -->

			</ul>
			<!-- /.nav navbar-nav navbar-right -->

		</div>
		<!-- /.collapse -->

	</div>
	<!-- /.container -->

</nav>
<!-- /.navbar -->