<div id="footer">

	<div class="container text-center">

		<div class="row">
			<div class="col-md-12">
				<p>© <?php echo date('Y'); ?> NEW COD3</p>
			</div>
		</div>

		<div class="row hidden">

			<div class="col-lg-4">
				<h4>My Bunker</h4>
				<p>
					Some Address 987,<br/>
					+34 9054 5455, <br/>
					Madrid, Spain.
				</p>
			</div>
			<!-- /col-lg-4 -->

			<div class="col-lg-4">
				<h4>Meus links</h4>
				<p>
					<a href="#">Facebook</a><br/>
					<a href="#">Twitter</a><br/>
					<a href="#">Dribbble</a>
				</p>
			</div>
			<!-- /col-lg-4 -->

			<div class="col-lg-4">
				<h4>About Stanley</h4>
				<p>This cute theme was created to showcase your work in a simple way. Use it wisely.</p>
			</div>
			<!-- /col-lg-4 -->

		</div>
		<!-- /.row -->

	</div>
	<!-- /.container -->

</div>
<!-- /#footer -->