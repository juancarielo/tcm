<?php $this->start('css');
echo $this->Html->css(array('../libs/wysihtml5/src/bootstrap-wysihtml5', '../libs/wysihtml5/lib/css/bootstrap3-wysiwyg5'));
$this->end(); ?>

<?php $this->start('script');
echo $this->Html->script(array('../libs/wysihtml5/lib/js/wysihtml5-0.3.0', '../libs/wysihtml5/src/bootstrap3-wysihtml5', '../libs/wysihtml5/src/locales/bootstrap-wysihtml5.pt-BR'));
$this->end(); ?>

<script>
	$(function() {
		$('textarea').wysihtml5({locale: "pt-BR"});
	});
</script>