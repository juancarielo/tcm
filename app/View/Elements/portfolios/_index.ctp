<?php $this->start('script');
#echo $this->Html->script(array('portfolio'));
$this->end(); ?>

<script>
	function openPortfolio(id){

		//alert(base_url+'/admin/view_ajax/'+id);

		$.ajax({
			type: "POST",
			url: base_url+'/admin/portfolios/view_ajax/'+id,
			datatype: "html",
			data: id,

			success: function(e) {

				//console.log(e);
				$('.modal-body').html(e);

			}

		});

		//$('.modal-body h2').html(id);

	// Infos do portfólio.
	/*$('.modal-body h2').html(id);
	$('.modal-body img').attr('src', );
	$('.modal-body p').html();
	// Detalhes do portfólio.
	$('.modal-body .cliente').html();
	$('.modal-body .data').html();
	$('.modal-body .servico').html();
	*/

}
</script>

<div class="container pt">
		<div class="row mt centered text-center">
			<div class="col-lg-4">
				<a class="zoom green" href="work01.html"><img class="img-responsive" src="img/portfolio/port01.jpg" alt="" /></a>
				<p>APE</p>
			</div>
			<div class="col-lg-4">
				<a class="zoom green" href="work01.html"><img class="img-responsive" src="img/portfolio/port02.jpg" alt="" /></a>
				<p>RAIDERS</p>
			</div>
			<div class="col-lg-4">
				<a class="zoom green" href="work01.html"><img class="img-responsive" src="img/portfolio/port03.jpg" alt="" /></a>
				<p>VIKINGS</p>
			</div>
		</div><!-- /row -->
		<div class="row mt centered text-center">
			<div class="col-lg-4">
				<a class="zoom green" href="work01.html"><img class="img-responsive" src="img/portfolio/port04.jpg" alt="" /></a>
				<p>YODA</p>
			</div>
			<div class="col-lg-4">
				<a class="zoom green" href="work01.html"><img class="img-responsive" src="img/portfolio/port05.jpg" alt="" /></a>
				<p>EMPERORS</p>
			</div>
			<div class="col-lg-4">
				<a class="zoom green" href="work01.html"><img class="img-responsive" src="img/portfolio/port06.jpg" alt="" /></a>
				<p>CHIEFS</p>
			</div>
		</div><!-- /row -->
	</div><!-- /container -->




<section id="portfolio" class="hidden">

	<div class="container">

		<div class="row">
			<div class="col-lg-12 text-center">
				<h2>Portfólio</h2>
				<hr class="star-primary">
			</div>
		</div>
		<!-- /.row -->

		<div class="row">

			<?php foreach ($portfolios as $portfolio): ?>

				<div class="col-sm-4 portfolio-item">

					<a href="#modal" onclick="openPortfolio(<?php echo $portfolio['Portfolio']['id']?>)" class="portfolio-link" data-toggle="modal">

						<div class="caption">
							<div class="caption-content">
								<?php echo $portfolio['Portfolio']['titulo']?>
								<br>
								<?php echo $portfolio['Portfolio']['created']?>
								<!-- <i class="fa fa-search-plus fa-3x"></i> -->
							</div>
						</div>

						<img src="<?php echo $portfolio['Portfolio']['img']?>" class="img-responsive" alt="" />

					</a>

				</div>
				<!-- /.col-sm-4 -->

			<?php endforeach ?>

		</div>
		<!-- /.row -->

	</div>
	<!-- /.containter -->

</section>
<!-- /#portfolio -->

<div class="portfolio-modal modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">

	<div class="modal-content">

		<div class="close-modal" data-dismiss="modal">
			<div class="lr">
				<div class="rl">
				</div>
			</div>
		</div>
		<!-- /.close-modal -->

		<div class="container">

			<div class="row">

				<div class="col-lg-8 col-lg-offset-2">

					<div class="modal-body">


					</div>
					<!-- /.modal-body -->

				</div>
				<!-- /.col-lg-8 -->

			</div>
			<!-- /.row -->

		</div>
		<!-- /.container -->

	</div>
	<!-- /.modal-content -->

</div>
<!-- /.portfolio-modal -->