<div class="container pt">

	<?php if ($this->request->params['controller'] == 'portfolios'): ?>
		<div class="row mt">
			<div class="col-lg-6 col-lg-offset-3 centered">
				<h3>PORTFÓLIOS</h3>
				<hr>
				<p>Show your work here. Dribbble shots from the awesome designer <a href="http://dribbble.com/wanderingbert">David Creighton-Pester</a>.</p>
			</div>
		</div>
	<?php endif ?>

	<div class="row mt centered">

		<?php foreach ($portfolios as $portfolio): ?>

			<div class="col-lg-4">
				<a class="zoom green" href="<?php echo $this->base.'/portfolio/'.$portfolio['Portfolio']['slug'] ?>">
					<img class="img-responsive" src="<?php echo $portfolio['Portfolio']['img']?>" alt="" />
				</a>
				<p><?php echo $portfolio['Portfolio']['titulo']?></p>
			</div>
			<!-- /.col-lg-4 -->

		<?php endforeach ?>

	</div>
	<!-- .row -->

</div>
<!-- /.container -->