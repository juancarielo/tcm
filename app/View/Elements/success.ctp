<div class="container">
	<div class="alert alert-success fade in">
		 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    <?php echo $message; ?>
	</div>
	<!-- /.alert -->
</div>
<!-- /.container -->
