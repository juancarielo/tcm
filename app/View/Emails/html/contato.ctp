<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="initial-scale=1.0">
	<meta name="format-detection" content="telephone=no">
	<style type="text/css">
		body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
		body {margin:0; padding:0;}
		table {border-spacing:0;}
		table td {border-collapse:collapse;}
		.ReadMsgBody { width: 100%; background-color: #ebebeb;}
		.ExternalClass {width: 100%; background-color: #ebebeb;}
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
		.yshortcuts a {border-bottom: none !important;}
		@media screen and (max-width: 600px) {
			table[class="container"] {
				width: 95% !important;
			}
		}
		@media screen and (max-width: 480px) {
			td[class="container-padding"] {
				padding-left: 12px !important;
				padding-right: 12px !important;
			}
		}
	</style>
</head>
<body style="margin:0; padding:10px 0;" bgcolor="#ebebeb" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

	<br>

	<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#ebebeb">
		<tr>
			<td align="center" valign="top" bgcolor="#ebebeb" style="background-color: #ebebeb;">

				<table border="0" width="600" cellpadding="0" cellspacing="0" class="container" bgcolor="#ffffff">

					<tr>

						<td class="container-padding" bgcolor="#ffffff" style="background-color: #ffffff; padding-left: 30px; padding-right: 30px; font-size: 14px; line-height: 20px; font-family: Helvetica, sans-serif; color: #333;">

							<br>

							<div style="font-weight: bold; font-size: 18px; line-height: 24px; color: #D03C0F">
								Contato
							</div>

							<p>
								<br>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.<br>
							</p>

							<p>Nome: <?php echo $nome; ?></p>
							<p>Assunto: <?php echo $assunto; ?></p>
							<p>Mensagem: <?php echo $mensagem; ?></p>

							<p>
								<em style="font-style:italic; font-size: 12px; color: #aaa;">Obrigado por entrar em conato, aguarde um retorno.</em>
								<br>
								<br>
							</p>
						</td>

					</tr>

				</table>

			</td>

		</tr>

	</table>

	<br>
	<br>

</body>
</html>

