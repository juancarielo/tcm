<div class="container">

	<div class="row">
		
		<div class="col-md-2 actions">

			<h3><?php echo __('Actions'); ?></h3>
			
			<ul class="nav nav-pills nav-stacked">

						<li><?php echo $this->Html->link(__('Edit Bio'), array('action' => 'edit', $bio['Bio']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Bio'), array('action' => 'delete', $bio['Bio']['id']), null, __('Are you sure you want to delete # %s?', $bio['Bio']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Bios'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bio'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Status'), array('controller' => 'status', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Status'), array('controller' => 'status', 'action' => 'add')); ?> </li>
			</ul>
			<!-- /.nav nav-pills nav-stacked -->

		</div>
		<!-- /.col-md-2 -->

		<div class="col-md-10">

			<?php 
			$this->Html->addCrumb('Home', '/admin');
			$this->Html->addCrumb(__('Bios'), '/admin/Bios');
			$this->Html->addCrumb(__('View') . " [{$this->request->params['pass'][0]}]" );
			echo $this->Html->getCrumbList(array('class' => 'breadcrumb', 'lastClass' => 'active'));
			?> 
			<!-- /.breadcrumb -->		

			<div class="bios view">

				<h2 class="space"><?php echo __('Bio'); ?></h2>

				<div class="panel panel-default space">

					<ul class="list-group">
								<li class='list-group-item'><strong><?php echo __('Id'); ?></strong>: <?php echo h($bio['Bio']['id']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Status'); ?></strong>: <?php echo $this->Html->link($bio['Status']['descricao'], array('controller' => 'status', 'action' => 'view', $bio['Status']['id'])); ?></li>
		<li class='list-group-item'><strong><?php echo __('Titulo'); ?></strong>: <?php echo h($bio['Bio']['titulo']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Descricao'); ?></strong>: <?php echo h($bio['Bio']['descricao']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Created'); ?></strong>: <?php echo h($bio['Bio']['created']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Modified'); ?></strong>: <?php echo h($bio['Bio']['modified']); ?></li>
					</ul>
					<!-- /.list-group -->

				</div>
				<!-- /.panel panel-default space -->

			</div>
			<!-- /.view -->
			
			
	</div>
	<!-- /.col-md-10 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->
