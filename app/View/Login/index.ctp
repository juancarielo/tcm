<style>
	/*body {padding-top: 200px !important; padding-bottom: 15px; background-color: #f5f5f5; text-align: center;}*/
	body {padding-top: 170px !important; padding-bottom: 15px; background-color: #d93921; text-align: center; color: #fff;}*
	.navbar {display: none;}
	#flashMessage{margin-top: 15px;}
</style>

<div class="col-md-4 col-md-offset-4">

	<?php

	echo $this->Form->create('', array('url' => array('controller' => 'login', 'action' => 'index'), 'class' => 'signin', 'inputDefaults' => array('label' => false, 'div' => array('class' => 'form-group'), 'class' => 'form-control')));

	#echo $this->Html->image("logo.png", array('style' => 'padding-bottom: 25px;', 'class' =>'img-responsive'));

	echo $this->Form->input('login', array('placeholder' => 'Login', 'autofocus'));

	echo $this->Form->input('senha', array('placeholder' => 'Senha', 'type' => 'password'));

	?>

	<div class="checkbox" style="text-align: left;">
		<label>
			<?php echo $this->Form->checkbox('manter', array('value' => 1)); ?>
			Continuar conectado
		</label>
	</div>

	<?php

	echo $this->Form->button('Acessar', array('class' => 'btn btn-default btn-block loading', 'data-loading-text' => 'Acessando..'));

	echo $this->Session->flash();

	echo $this->Form->end();

	?>

</div>
<!-- /.col-md-6 col-md-offset-3 -->