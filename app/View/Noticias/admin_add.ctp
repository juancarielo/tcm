<?php echo $this->element('plugins/wysihtml5'); ?>

<div class="container">

	<div class="row">
		<div class="col-md-12">
			<?php
			$this->Html->addCrumb('Home', '/admin');
			$this->Html->addCrumb(__('Notícias'), '/admin/noticias');
			$this->Html->addCrumb(__('Add'));
			echo $this->Html->getCrumbList(array('class' => 'breadcrumb', 'lastClass' => 'active'));
			?>
			<!-- /.breadcrumb -->
		</div>
	</div>

	<div class="row">

		<div class="col-md-12">

			<div class="noticias form">

				<?php echo $this->Form->create('Noticia', array(
					'role' => 'form',
					'inputDefaults' => array(
						'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
						'div' => array('class' => 'form-group'),
						'class' => 'form-control',
						'label' => array('class' => 'control-label'),
						'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
						))); ?>

						<fieldset>
							<legend><?php echo __('Adicionar notícia'); ?></legend>
							<?php
							echo $this->Form->input('titulo');
							echo $this->Form->input('descricao', array('type' => 'textarea'));
							?>
						</fieldset>

						<div class="space"><?php echo $this->Form->button (__('Submit'),array('type' => 'submit', 'class' => 'btn btn-primary', 'div' => false, 'style' => 'margin-right: 10px;')); ?><?php echo $this->Html->link(__('Cancel'), array('action' => 'index'), array('class' => 'btn btn-primary', 'div' => false)); ?></div>
						<!-- /.space --><?php echo $this->Form->end(); ?>

					</div>
					<!-- /.form -->

				</div>
				<!-- /.col-md-10 -->

			</div>
			<!-- /.row -->

		</div>
		<!-- /.container -->

