<div class="container">

	<div class="row">

		<div class="col-md-2 actions">

			<h3><?php echo __('Actions'); ?></h3>

			<ul class="nav nav-pills nav-stacked">
				<li><?php echo $this->Html->link(__('New Usuario'), array('action' => 'add')); ?></li>
			</ul>
			<!-- /.nav nav-pills nav-stacked -->

		</div>
		<!-- /.col-md-2 -->

		<div class="col-md-10">

			<?php
			$this->Html->addCrumb('Home', '/admin');
			$this->Html->addCrumb(__('Usuarios'));
			echo $this->Html->getCrumbList(array('class' => 'breadcrumb', 'lastClass' => 'active'));
			?>
			<!-- /.breadcrumb -->

			<h2 class="space"><?php echo __('Usuarios'); ?></h2>

			<?php if (!$usuarios): ?>
				<p><?php echo __('No records found.'); ?></p>

			<?php else: ?>
				<div class="usuarios index">



					<div class="table-responsive">

						<table class="table table-condensed table-striped table-bordered">

							<thead>
								<tr>
									<th><?php echo $this->Paginator->sort('id'); ?></th>
									<th><?php echo $this->Paginator->sort('status_id'); ?></th>
									<th><?php echo $this->Paginator->sort('tipo_id'); ?></th>
									<th><?php echo $this->Paginator->sort('nome'); ?></th>
									<th><?php echo $this->Paginator->sort('email'); ?></th>
									<th><?php echo $this->Paginator->sort('created'); ?></th>
									<th><?php echo $this->Paginator->sort('modified'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>

							<tbody>
								<?php foreach ($usuarios as $usuario): ?>
									<tr>
										<td><?php echo h($usuario['Usuario']['id']); ?></td>
										<td>
											<?php echo $this->Html->link($usuario['Status']['descricao'], array('controller' => 'status', 'action' => 'view', $usuario['Status']['id'])); ?>
										</td>
										<td>
											<?php echo $this->Html->link($usuario['Tipo']['descricao'], array('controller' => 'tipos', 'action' => 'view', $usuario['Tipo']['id'])); ?>
										</td>
										<td><?php echo h($usuario['Usuario']['nome']); ?></td>
										<td><?php echo h($usuario['Usuario']['email']); ?></td>
										<td><?php echo h($usuario['Usuario']['created']); ?></td>
										<td><?php echo h($usuario['Usuario']['modified']); ?></td>
										<td class="actions">
											<?php echo $this->Html->link('<span class="glyphicon glyphicon-eye-open"></span>', array('action' => 'view', $usuario['Usuario']['id']), array('escape' => false, 'class' => 'btn btn-default btn-xs', 'rel' => 'tooltip', 'data-original-title' => 'Visualizar')); ?>
											<?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', array('action' => 'edit', $usuario['Usuario']['id']), array('escape' => false, 'class' => 'btn btn-default btn-xs', 'rel' => 'tooltip', 'data-original-title' => 'Editar')); ?>
											<?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span>', array('action' => 'delete', $usuario['Usuario']['id']), array('escape' => false, 'class' => 'btn btn-default btn-xs', 'rel' => 'tooltip', 'data-original-title' => 'Excluir'), __('Are you sure you want to delete # %s?', $usuario['Usuario']['id'])); ?>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>

						</table>
						<!-- .table -->

					</div>
					<!-- /.table-responsive -->

					<p>
						<?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')	));	?>
					</p>
					<!-- /.paginator-counter -->

					<ul class="pagination">
						<?php
						echo $this->Paginator->prev('«', array('tag' => 'li'), null, array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'a'));
						echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
						echo $this->Paginator->next('»', array('tag' => 'li'), null, array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'a'));
						?>
					</ul>
					<!-- /.pagination -->

				</div>
				<!-- /.index -->

			<?php endif; ?>
		</div>
		<!-- /.col-md-10 -->

	</div>
	<!-- /.row -->

</div>
<!-- /.container -->
