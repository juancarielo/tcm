<div class="container">

	<div class="row">
		
		<div class="col-md-2 actions">

			<h3><?php echo __('Actions'); ?></h3>
			
			<ul class="nav nav-pills nav-stacked">

						<li><?php echo $this->Html->link(__('Edit Usuario'), array('action' => 'edit', $usuario['Usuario']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Usuario'), array('action' => 'delete', $usuario['Usuario']['id']), null, __('Are you sure you want to delete # %s?', $usuario['Usuario']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Usuarios'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Usuario'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Status'), array('controller' => 'status', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Status'), array('controller' => 'status', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipos'), array('controller' => 'tipos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipo'), array('controller' => 'tipos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Logs'), array('controller' => 'logs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Log'), array('controller' => 'logs', 'action' => 'add')); ?> </li>
			</ul>
			<!-- /.nav nav-pills nav-stacked -->

		</div>
		<!-- /.col-md-2 -->

		<div class="col-md-10">

			<?php 
			$this->Html->addCrumb('Home', '/admin');
			$this->Html->addCrumb(__('Usuarios'), '/admin/Usuarios');
			$this->Html->addCrumb(__('View') . " [{$this->request->params['pass'][0]}]" );
			echo $this->Html->getCrumbList(array('class' => 'breadcrumb', 'lastClass' => 'active'));
			?> 
			<!-- /.breadcrumb -->		

			<div class="usuarios view">

				<h2 class="space"><?php echo __('Usuario'); ?></h2>

				<div class="panel panel-default space">

					<ul class="list-group">
								<li class='list-group-item'><strong><?php echo __('Id'); ?></strong>: <?php echo h($usuario['Usuario']['id']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Status'); ?></strong>: <?php echo $this->Html->link($usuario['Status']['descricao'], array('controller' => 'status', 'action' => 'view', $usuario['Status']['id'])); ?></li>
		<li class='list-group-item'><strong><?php echo __('Tipo'); ?></strong>: <?php echo $this->Html->link($usuario['Tipo']['descricao'], array('controller' => 'tipos', 'action' => 'view', $usuario['Tipo']['id'])); ?></li>
		<li class='list-group-item'><strong><?php echo __('Nome'); ?></strong>: <?php echo h($usuario['Usuario']['nome']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Email'); ?></strong>: <?php echo h($usuario['Usuario']['email']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Senha'); ?></strong>: <?php echo h($usuario['Usuario']['senha']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Created'); ?></strong>: <?php echo h($usuario['Usuario']['created']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Modified'); ?></strong>: <?php echo h($usuario['Usuario']['modified']); ?></li>
					</ul>
					<!-- /.list-group -->

				</div>
				<!-- /.panel panel-default space -->

			</div>
			<!-- /.view -->
			
			
			<div class="related">

				<h3><?php echo __('Related Logs'); ?></h3>

				<?php if (!empty($usuario['Log'])): ?>

				<div class="table-responsive">

					<table class="table table-condensed table-striped table-bordered">
						
						<thead>
							<tr>
										<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Usuario Id'); ?></th>
		<th><?php echo __('Registro Id'); ?></th>
		<th><?php echo __('Tabela'); ?></th>
		<th><?php echo __('Operacao'); ?></th>
		<th><?php echo __('Dados Usuario'); ?></th>
		<th><?php echo __('Observacoes'); ?></th>
		<th><?php echo __('Created'); ?></th>
								<th class="actions"><?php echo __('Actions'); ?></th>
							</tr>
						</thead>

						<tbody>
								<?php foreach ($usuario['Log'] as $log): ?>
		<tr>
			<td><?php echo $log['id']; ?></td>
			<td><?php echo $log['usuario_id']; ?></td>
			<td><?php echo $log['registro_id']; ?></td>
			<td><?php echo $log['tabela']; ?></td>
			<td><?php echo $log['operacao']; ?></td>
			<td><?php echo $log['dados_usuario']; ?></td>
			<td><?php echo $log['observacoes']; ?></td>
			<td><?php echo $log['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('<span class="glyphicon glyphicon-eye-open"></span>'), array('controller' => 'logs', 'action' => 'view', $log['id']), array('escape' => false, 'class' => 'btn btn-default btn-xs', 'rel' => 'tooltip', 'data-original-title' => 'Visualizar')); ?>
				<?php echo $this->Html->link(__('<span class="glyphicon glyphicon-edit"></span>'), array('controller' => 'logs', 'action' => 'edit', $log['id']), array('escape' => false, 'class' => 'btn btn-default btn-xs', 'rel' => 'tooltip', 'data-original-title' => 'Editar')); ?>
				<?php echo $this->Form->postLink(__('<span class="glyphicon glyphicon-trash"></span>'), array('controller' => 'logs', 'action' => 'delete', $log['id']), array('escape' => false, 'class' => 'btn btn-default btn-xs', 'rel' => 'tooltip', 'data-original-title' => 'Excluir'), __('Are you sure you want to delete # %s?', $log['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
						</tbody>

					</table>
					<!-- /.table -->

				</div>
				<!-- /.table-responsive -->

				<?php endif; ?>


				<div class="actions">
					<ul>
						<li><?php echo $this->Html->link(__('New Log'), array('controller' => 'logs', 'action' => 'add')); ?> </li>
					</ul>
				</div>
				<!-- /.actions -->

			</div>
			<!-- /.related -->

		
	</div>
	<!-- /.col-md-10 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->
