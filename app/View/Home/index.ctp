<div class="container mtb">

	<div class="row">

		<div class="col-md-8">

			<div class="spacing"></div>

			<?php echo $this->element('tmp/noticias'); ?>

		</div>

		<div class="col-md-4">

			<div id="box">
				<h4>Login</h4>
				<div class="hline"></div>
				<br>
				<form role="form">

					<div class="form-group">
						<input type="email" placeholder="E-mail" class="form-control" id="exampleInputEmail1">
					</div>

					<div class="form-group">
						<input type="password" placeholder="Senha" class="form-control" id="exampleInputEmail2">
					</div>

					<button type="submit" class="btn btn-block btn-theme">Login</button>
				</form>
			</div>
			<!-- /#box -->

			<div class="spacing"></div>

			<div id="box">

				<h4>Parceiros</h4>

				<div class="hline"></div>

				<?php for ($i=0; $i < 4; $i++): ?>

					<p><img class="img-responsive" src="http://placehold.it/600x350" alt=""></p>

				<?php endfor; ?>

			</div>
			<!-- /#box -->

		</div>

	</div>

</div>