<div class="container">

	<div class="row">
		
		<div class="col-md-2 actions">

			<h3><?php echo __('Actions'); ?></h3>
			
			<ul class="nav nav-pills nav-stacked">

						<li><?php echo $this->Html->link(__('Edit Portfolio'), array('action' => 'edit', $portfolio['Portfolio']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Portfolio'), array('action' => 'delete', $portfolio['Portfolio']['id']), null, __('Are you sure you want to delete # %s?', $portfolio['Portfolio']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Portfolios'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Portfolio'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Status'), array('controller' => 'status', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Status'), array('controller' => 'status', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clientes'), array('controller' => 'clientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cliente'), array('controller' => 'clientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Imagens'), array('controller' => 'imagens', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Imagem'), array('controller' => 'imagens', 'action' => 'add')); ?> </li>
			</ul>
			<!-- /.nav nav-pills nav-stacked -->

		</div>
		<!-- /.col-md-2 -->

		<div class="col-md-10">

			<?php 
			$this->Html->addCrumb('Home', '/admin');
			$this->Html->addCrumb(__('Portfolios'), '/admin/Portfolios');
			$this->Html->addCrumb(__('View') . " [{$this->request->params['pass'][0]}]" );
			echo $this->Html->getCrumbList(array('class' => 'breadcrumb', 'lastClass' => 'active'));
			?> 
			<!-- /.breadcrumb -->		

			<div class="portfolios view">

				<h2 class="space"><?php echo __('Portfolio'); ?></h2>

				<div class="panel panel-default space">

					<ul class="list-group">
								<li class='list-group-item'><strong><?php echo __('Id'); ?></strong>: <?php echo h($portfolio['Portfolio']['id']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Status'); ?></strong>: <?php echo $this->Html->link($portfolio['Status']['descricao'], array('controller' => 'status', 'action' => 'view', $portfolio['Status']['id'])); ?></li>
		<li class='list-group-item'><strong><?php echo __('Cliente'); ?></strong>: <?php echo $this->Html->link($portfolio['Cliente']['descricao'], array('controller' => 'clientes', 'action' => 'view', $portfolio['Cliente']['id'])); ?></li>
		<li class='list-group-item'><strong><?php echo __('Titulo'); ?></strong>: <?php echo h($portfolio['Portfolio']['titulo']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Descricao'); ?></strong>: <?php echo h($portfolio['Portfolio']['descricao']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Url'); ?></strong>: <?php echo h($portfolio['Portfolio']['url']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Slug'); ?></strong>: <?php echo h($portfolio['Portfolio']['slug']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Img'); ?></strong>: <?php echo h($portfolio['Portfolio']['img']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Created'); ?></strong>: <?php echo h($portfolio['Portfolio']['created']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Modified'); ?></strong>: <?php echo h($portfolio['Portfolio']['modified']); ?></li>
					</ul>
					<!-- /.list-group -->

				</div>
				<!-- /.panel panel-default space -->

			</div>
			<!-- /.view -->
			
			
			<div class="related">

				<h3><?php echo __('Related Imagens'); ?></h3>

				<?php if (!empty($portfolio['Imagem'])): ?>

				<div class="table-responsive">

					<table class="table table-condensed table-striped table-bordered">
						
						<thead>
							<tr>
										<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Status Id'); ?></th>
		<th><?php echo __('Portfolio Id'); ?></th>
		<th><?php echo __('Img'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
								<th class="actions"><?php echo __('Actions'); ?></th>
							</tr>
						</thead>

						<tbody>
								<?php foreach ($portfolio['Imagem'] as $imagem): ?>
		<tr>
			<td><?php echo $imagem['id']; ?></td>
			<td><?php echo $imagem['status_id']; ?></td>
			<td><?php echo $imagem['portfolio_id']; ?></td>
			<td><?php echo $imagem['img']; ?></td>
			<td><?php echo $imagem['created']; ?></td>
			<td><?php echo $imagem['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('<span class="glyphicon glyphicon-eye-open"></span>'), array('controller' => 'imagens', 'action' => 'view', $imagem['id']), array('escape' => false, 'class' => 'btn btn-default btn-xs', 'rel' => 'tooltip', 'data-original-title' => 'Visualizar')); ?>
				<?php echo $this->Html->link(__('<span class="glyphicon glyphicon-edit"></span>'), array('controller' => 'imagens', 'action' => 'edit', $imagem['id']), array('escape' => false, 'class' => 'btn btn-default btn-xs', 'rel' => 'tooltip', 'data-original-title' => 'Editar')); ?>
				<?php echo $this->Form->postLink(__('<span class="glyphicon glyphicon-trash"></span>'), array('controller' => 'imagens', 'action' => 'delete', $imagem['id']), array('escape' => false, 'class' => 'btn btn-default btn-xs', 'rel' => 'tooltip', 'data-original-title' => 'Excluir'), __('Are you sure you want to delete # %s?', $imagem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
						</tbody>

					</table>
					<!-- /.table -->

				</div>
				<!-- /.table-responsive -->

				<?php endif; ?>


				<div class="actions">
					<ul>
						<li><?php echo $this->Html->link(__('New Imagem'), array('controller' => 'imagens', 'action' => 'add')); ?> </li>
					</ul>
				</div>
				<!-- /.actions -->

			</div>
			<!-- /.related -->

		
	</div>
	<!-- /.col-md-10 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->
