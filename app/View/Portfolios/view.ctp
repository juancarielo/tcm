<div class="container pt">

	<div class="row">

	</div>

	<div class="row">

		<div class="col-lg-6 col-lg-offset-3 centered">
			<h3><?php echo $portfolio['Portfolio']['titulo'] ?></h3>
			<hr>
			<p><?php echo $portfolio['Portfolio']['descricao'] ?></p>
		</div>
		<!-- /.col-lg-6 -->

	</div>
	<!-- /.row -->

	<div class="row centered">

		<div class="col-lg-8 col-lg-offset-2">

			<p>
				<bt>Cliente: <a target="_blank" href="<?php echo $portfolio['Portfolio']['url'] ?>"><?php echo $portfolio['Portfolio']['titulo'] ?></a></bt> -
				<bt>Tipo: <a href="#">Illustration</a></bt> -
				<bt>Data: <a href="#"><?php echo $portfolio['Portfolio']['created'] ?></a></bt>
			</p>

			<p><?php echo $this->Html->image('portfolio/1.jpg', array('class' => 'img-responsive')) ?></p>
			<p><?php echo $this->Html->image('portfolio/2.jpg', array('class' => 'img-responsive')) ?></p>
			<p><?php echo $this->Html->image('portfolio/3.jpg', array('class' => 'img-responsive')) ?></p>

		</div>
		<!-- /.col-lg-8 -->

	</div>
	<!-- /row -->

</div>
<!-- /container -->