<h2><?php echo $portfolio['Portfolio']['titulo'];?></h2>

<hr class="star-primary">

<img src="<?php echo $portfolio['Portfolio']['img'];?>" class="img-responsive img-centered" alt="">

<p><?php echo $portfolio['Portfolio']['descricao'];?></p>

<ul class="list-inline item-details">
	<li class="cliente">Cliente: <strong><a href="http://startbootstrap.com"><?php echo $portfolio['Portfolio']['titulo'];?></a></strong></li>
	<li class="data">Data: <strong><?php echo $portfolio['Portfolio']['created'];?></strong></li>
	<li class="servico">URL: <strong><a href="<?php echo $portfolio['Portfolio']['url'];?>"><?php echo $portfolio['Portfolio']['url'];?></a></strong></li>
</ul>

<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>