<?php echo $this->element('plugins/wysihtml5'); ?>

<div class="container">

	<div class="row">

		<div class="col-md-2 actions">

			<h3><?php echo __('Actions'); ?></h3>

			<ul class="nav nav-pills nav-stacked">

				<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Portfolio.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Portfolio.id'))); ?></li>
				<li><?php echo $this->Html->link(__('List Portfolios'), array('action' => 'index')); ?></li>
				<li><?php echo $this->Html->link(__('List Status'), array('controller' => 'status', 'action' => 'index')); ?> </li>
				<li><?php echo $this->Html->link(__('New Status'), array('controller' => 'status', 'action' => 'add')); ?> </li>
				<li><?php echo $this->Html->link(__('List Clientes'), array('controller' => 'clientes', 'action' => 'index')); ?> </li>
				<li><?php echo $this->Html->link(__('New Cliente'), array('controller' => 'clientes', 'action' => 'add')); ?> </li>
				<li><?php echo $this->Html->link(__('List Imagens'), array('controller' => 'imagens', 'action' => 'index')); ?> </li>
				<li><?php echo $this->Html->link(__('New Imagem'), array('controller' => 'imagens', 'action' => 'add')); ?> </li>
			</ul>
			<!-- /.nav nav-pills nav-stacked -->

		</div>
		<!-- /.col-md-2 -->

		<div class="col-md-10">

			<?php
			$this->Html->addCrumb('Home', '/admin');
			$this->Html->addCrumb(__('Portfolios'), '/admin/Portfolios');
			$this->Html->addCrumb(__('Add'));
			echo $this->Html->getCrumbList(array('class' => 'breadcrumb', 'lastClass' => 'active'));
			?>
			<!-- /.breadcrumb -->

			<div class="portfolios form">

				<?php echo $this->Form->create('Portfolio', array(
					'role' => 'form',
					'enctype' => 'multipart/form-data',
					'inputDefaults' => array(
						'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
						'div' => array('class' => 'form-group'),
						'class' => 'form-control',
						'label' => array('class' => 'control-label'),
						'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
						))); ?>

						<fieldset>
							<legend><?php echo __('Admin Edit Portfolio'); ?></legend>
							<?php
							echo $this->Form->input('id');
							echo $this->Form->input('status_id');
							echo $this->Form->input('cliente_id');
							echo $this->Form->input('titulo');
							echo $this->Form->input('descricao');
							#echo $this->Form->input('url');
							#echo $this->Form->input('slug');
							echo $this->Form->input('img', array('type' => 'file'));
							?>
							<img src="<?php echo $this->base.'/'.$this->data['Portfolio']['img']; ?>" width="250" alt="">

						</fieldset>

						<div class="space"><?php echo $this->Form->button (__('Submit'),array('type' => 'submit', 'class' => 'btn btn-primary', 'div' => false, 'style' => 'margin-right: 10px;')); ?><?php echo $this->Html->link(__('Cancel'), array('action' => 'index'), array('class' => 'btn btn-primary', 'div' => false)); ?></div>
						<!-- /.space --><?php echo $this->Form->end(); ?>

					</div>
					<!-- /.form -->

				</div>
				<!-- /.col-md-10 -->

			</div>
			<!-- /.row -->

		</div>
		<!-- /.container -->

