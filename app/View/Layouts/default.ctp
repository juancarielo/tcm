<!DOCTYPE html>

<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->

<head>

	<title><?php echo $title_for_layout; ?></title>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

	<?php

	echo $this->Html->charset();
	echo $this->Html->meta('icon');

	echo $this->Html->css(array('bootstrap', 'style', 'main', 'font-awesome.min'));
	echo $this->Html->script(array('modernizr.min'));

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');

	?>

</head>

<body>

	<div id="menu">
		<?php echo $this->element('menus/default'); ?>
	</div>
	<!-- /#menu -->

	<div id="content">
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>
	</div>
	<!-- /#content -->


	<div id="footerwrap">

		<div class="container">

			<div class="row">

				<div class="col-lg-12 text-center">
					<p>© 2014 DWG COMPANY</p>
				</div>

			</div>

		</div>

	</div>

	<?php

	echo $this->Html->script(array('jquery.min', 'bootstrap.min', 'retina-1.1.0', 'jquery.hoverdir', 'jquery.hoverex.min', 'jquery.prettyPhoto', 'jquery.isotope.min', 'custom'));
	echo $this->fetch('script');

	?>

</body>
</html>