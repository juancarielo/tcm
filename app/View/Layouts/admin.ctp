<!DOCTYPE html>

<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>

    <title><?php echo $title_for_layout; ?></title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width" />

    <?php

    echo $this->Html->charset();
    echo $this->Html->meta('icon');

    echo $this->Html->css(array('admin/lumen.min'));
    echo $this->Html->script(array('admin/vendor/jquery.min', 'admin/vendor/modernizr.min', 'admin/vendor/bootstrap.min', 'admin/main'));

    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');

    ?>

</head>
<body style="padding-top: 70px;">

    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <script>
            var base_url = "<?php echo $this->base;?>/admin/";
        </script>

        <style>
            .space {margin: 20px 0;}
        </style>

        <div id="menu">
            <?php echo $this->element('menus/admin'); ?>
        </div>
        <!-- /#menu -->

        <div class="container">

            <div class="row">

                <div id="content">

                    <?php echo $this->Session->flash(); ?>
                    <?php echo $this->fetch('content'); ?>

                </div>
                <!-- /#content -->

            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->

        <?php #echo $this->element('google_analytics'); ?>
        <?php #echo $this->element('sql_dump'); ?>

    </body>
    </html>