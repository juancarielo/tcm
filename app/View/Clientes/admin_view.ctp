<div class="container">

	<div class="row">
		
		<div class="col-md-2 actions">

			<h3><?php echo __('Actions'); ?></h3>
			
			<ul class="nav nav-pills nav-stacked">

						<li><?php echo $this->Html->link(__('Edit Cliente'), array('action' => 'edit', $cliente['Cliente']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Cliente'), array('action' => 'delete', $cliente['Cliente']['id']), null, __('Are you sure you want to delete # %s?', $cliente['Cliente']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Clientes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cliente'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Status'), array('controller' => 'status', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Status'), array('controller' => 'status', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Portfolios'), array('controller' => 'portfolios', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Portfolio'), array('controller' => 'portfolios', 'action' => 'add')); ?> </li>
			</ul>
			<!-- /.nav nav-pills nav-stacked -->

		</div>
		<!-- /.col-md-2 -->

		<div class="col-md-10">

			<?php 
			$this->Html->addCrumb('Home', '/admin');
			$this->Html->addCrumb(__('Clientes'), '/admin/Clientes');
			$this->Html->addCrumb(__('View') . " [{$this->request->params['pass'][0]}]" );
			echo $this->Html->getCrumbList(array('class' => 'breadcrumb', 'lastClass' => 'active'));
			?> 
			<!-- /.breadcrumb -->		

			<div class="clientes view">

				<h2 class="space"><?php echo __('Cliente'); ?></h2>

				<div class="panel panel-default space">

					<ul class="list-group">
								<li class='list-group-item'><strong><?php echo __('Id'); ?></strong>: <?php echo h($cliente['Cliente']['id']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Status'); ?></strong>: <?php echo $this->Html->link($cliente['Status']['descricao'], array('controller' => 'status', 'action' => 'view', $cliente['Status']['id'])); ?></li>
		<li class='list-group-item'><strong><?php echo __('Descricao'); ?></strong>: <?php echo h($cliente['Cliente']['descricao']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Email'); ?></strong>: <?php echo h($cliente['Cliente']['email']); ?></li>
		<li class='list-group-item'><strong><?php echo __('Created'); ?></strong>: <?php echo h($cliente['Cliente']['created']); ?></li>
					</ul>
					<!-- /.list-group -->

				</div>
				<!-- /.panel panel-default space -->

			</div>
			<!-- /.view -->
			
			
			<div class="related">

				<h3><?php echo __('Related Portfolios'); ?></h3>

				<?php if (!empty($cliente['Portfolio'])): ?>

				<div class="table-responsive">

					<table class="table table-condensed table-striped table-bordered">
						
						<thead>
							<tr>
										<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Status Id'); ?></th>
		<th><?php echo __('Cliente Id'); ?></th>
		<th><?php echo __('Titulo'); ?></th>
		<th><?php echo __('Descricao'); ?></th>
		<th><?php echo __('Url'); ?></th>
		<th><?php echo __('Slug'); ?></th>
		<th><?php echo __('Img'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
								<th class="actions"><?php echo __('Actions'); ?></th>
							</tr>
						</thead>

						<tbody>
								<?php foreach ($cliente['Portfolio'] as $portfolio): ?>
		<tr>
			<td><?php echo $portfolio['id']; ?></td>
			<td><?php echo $portfolio['status_id']; ?></td>
			<td><?php echo $portfolio['cliente_id']; ?></td>
			<td><?php echo $portfolio['titulo']; ?></td>
			<td><?php echo $portfolio['descricao']; ?></td>
			<td><?php echo $portfolio['url']; ?></td>
			<td><?php echo $portfolio['slug']; ?></td>
			<td><?php echo $portfolio['img']; ?></td>
			<td><?php echo $portfolio['created']; ?></td>
			<td><?php echo $portfolio['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('<span class="glyphicon glyphicon-eye-open"></span>'), array('controller' => 'portfolios', 'action' => 'view', $portfolio['id']), array('escape' => false, 'class' => 'btn btn-default btn-xs', 'rel' => 'tooltip', 'data-original-title' => 'Visualizar')); ?>
				<?php echo $this->Html->link(__('<span class="glyphicon glyphicon-edit"></span>'), array('controller' => 'portfolios', 'action' => 'edit', $portfolio['id']), array('escape' => false, 'class' => 'btn btn-default btn-xs', 'rel' => 'tooltip', 'data-original-title' => 'Editar')); ?>
				<?php echo $this->Form->postLink(__('<span class="glyphicon glyphicon-trash"></span>'), array('controller' => 'portfolios', 'action' => 'delete', $portfolio['id']), array('escape' => false, 'class' => 'btn btn-default btn-xs', 'rel' => 'tooltip', 'data-original-title' => 'Excluir'), __('Are you sure you want to delete # %s?', $portfolio['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
						</tbody>

					</table>
					<!-- /.table -->

				</div>
				<!-- /.table-responsive -->

				<?php endif; ?>


				<div class="actions">
					<ul>
						<li><?php echo $this->Html->link(__('New Portfolio'), array('controller' => 'portfolios', 'action' => 'add')); ?> </li>
					</ul>
				</div>
				<!-- /.actions -->

			</div>
			<!-- /.related -->

		
	</div>
	<!-- /.col-md-10 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->
