<div class="container">

	<div class="row">

		<div class="col-md-12">

			<?php
			$this->Html->addCrumb('Home', '/admin');
			$this->Html->addCrumb(__('Clientes'));
			echo $this->Html->getCrumbList(array('class' => 'breadcrumb', 'lastClass' => 'active'));
			?>
			<!-- /.breadcrumb -->

			<h2 class="space">
				<div class="clearfix">
					<div class="pull-left">
						<?php echo __('Clientes'); ?>
					</div>

					<div class="pull-right">
						<small><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-user"></span> Novo cliente'), array('action' => 'add'), array('escape' => false, 'class' => 'btn btn-default')); ?></small>
					</div>
				</div>
			</h2>

			<?php if (!$clientes): ?>
				<p><?php echo __('No records found.'); ?></p>
			<?php else: ?>

				<div class="clientes index">

					<div class="table-responsive">

						<table class="table table-condensed table-striped table-bordered">

							<thead>
								<tr>
									<th><?php echo $this->Paginator->sort('id'); ?></th>
									<th><?php echo $this->Paginator->sort('status_id'); ?></th>
									<th><?php echo $this->Paginator->sort('descricao'); ?></th>
									<th><?php echo $this->Paginator->sort('email'); ?></th>
									<th><?php echo $this->Paginator->sort('created'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>

							<tbody>
								<?php foreach ($clientes as $cliente): ?>
									<tr>
										<td><?php echo h($cliente['Cliente']['id']); ?></td>
										<td>
											<?php echo $this->Html->link($cliente['Status']['descricao'], array('controller' => 'status', 'action' => 'view', $cliente['Status']['id'])); ?>
										</td>
										<td><?php echo h($cliente['Cliente']['descricao']); ?></td>
										<td><?php echo h($cliente['Cliente']['email']); ?></td>
										<td><?php echo h($cliente['Cliente']['created']); ?></td>
										<td class="actions">
											<?php echo $this->Html->link('<span class="glyphicon glyphicon-eye-open"></span>', array('action' => 'view', $cliente['Cliente']['id']), array('escape' => false, 'class' => 'btn btn-default btn-xs', 'rel' => 'tooltip', 'data-original-title' => 'Visualizar')); ?>
											<?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', array('action' => 'edit', $cliente['Cliente']['id']), array('escape' => false, 'class' => 'btn btn-default btn-xs', 'rel' => 'tooltip', 'data-original-title' => 'Editar')); ?>
											<?php echo $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span>', array('action' => 'delete', $cliente['Cliente']['id']), array('escape' => false, 'class' => 'btn btn-default btn-xs', 'rel' => 'tooltip', 'data-original-title' => 'Excluir'), __('Are you sure you want to delete # %s?', $cliente['Cliente']['id'])); ?>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>

						</table>
						<!-- .table -->

					</div>
					<!-- /.table-responsive -->

					<p>
						<?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')	));	?>
					</p>
					<!-- /.paginator-counter -->

					<ul class="pagination">
						<?php
						echo $this->Paginator->prev('«', array('tag' => 'li'), null, array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'a'));
						echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
						echo $this->Paginator->next('»', array('tag' => 'li'), null, array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'a'));
						?>
					</ul>
					<!-- /.pagination -->

				</div>
				<!-- /.index -->

			<?php endif; ?>
		</div>
		<!-- /.col-md-10 -->

	</div>
	<!-- /.row -->

</div>
<!-- /.container -->
